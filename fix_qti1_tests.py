import argparse
import io
import os.path
import shutil
import subprocess
import zipfile
from lxml import etree


def extract_qti(qtifile):
    with zipfile.ZipFile(qtifile, mode="r") as qtizip:
        with qtizip.open("qti.xml") as qtixml:
            tree = etree.parse(qtixml)
    for elem in tree.findall(".//item"):
        # XXX check if itemcontrol is there already
        etree.SubElement(
            elem,
            "itemcontrol",
            feedbackswitch="No",
            hintswitch="No",
            solutionswitch="No",
        )
    return etree.tostring(tree)


def main():
    parser = argparse.ArgumentParser(description="Fix OLAT QTI tests")
    parser.add_argument("qtifile")
    args = parser.parse_args()
    qti_xml = extract_qti(args.qtifile)
    with open("qti.xml", "w") as f:
        f.write(qti_xml.decode("utf-8"))

    qti_filename, qti_file_extension = os.path.splitext(args.qtifile)
    qti_file_fixed = f"{ qti_filename }_FIXED{ qti_file_extension }"
    shutil.copy(args.qtifile, qti_file_fixed)

    subprocess.run(["zip", qti_file_fixed, "qti.xml"])


if __name__ == "__main__":
    main()
